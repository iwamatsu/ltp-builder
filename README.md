# ltp builder

CI to build LTP binaries for Debian.

# CI variables

## for .build_ltp extend

  * SSH_PRIVATE_KEY: SSH key of artifact server
  * ARTIFACTS_SERVER: artifact server name (IP address, FQDN)
  * ARTIFACTS_SERVER_PORT: SSH port of artifact server
  * ARTIFACTS_PATH: Full path in the artifact server to store artifacts
  * DEBIAN_VERSION: Target Debian version (debian/11, 12 ....)
  * LATEST_TAG: a release tag saved in ltp.version

## for .trigger extend

  * LTP_GITLAB_USER: name of gitlab token
  * LTP_GITLAB_TOKEN: gitlab token
  * GIT_EMAIL: Email address used in Git
  * GIT_NAME}: User name used in Git

# License

  * Apache 2.0
  * Copyright: Nobuhiro Iwamatsu <iwamatsu@nigauri.org>
